<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('partials/head.php'); ?>
		<style>
		.slider-section{
			position: relative;
		}
		.slider-content-1{
			position: relative;
			left:300px;
			bottom: 10px;
		}
		.slider-content-2{
			position: relative;
			bottom: 620px;
			left: 50px
		}
		.slider-content-3{
			position: relative;
			bottom:830px;
			left:370px;
		}
		.slider-content-4{
			position: relative;
			bottom: 1150px;
			left:90px;
		}
		</style>
	</head>
	<body>
		<?php include('partials/brand.php'); ?>
		<section class="container section-menu">
			<div class="columns">
				<div class="column col-md-8 col-sm-12">
					<ul class="bxslider" style="margin:0;">
						<li><img src="img/mainslider-1.jpg" title="Funky roots" /></li>
						<li><img src="img/mainslider-2.jpg" title="The long and winding road" /></li>
						<li><img src="img/mainslider-3.jpg" title="Happy trees" /></li>
					</ul>
					<span id="slider-prev"></span>
					<span id="slider-next"></span>
				</div>
				<div class="column col-md-4">
					<?php include('partials/main-menu.php'); ?>
				</div>
			</div>
		</section>
		<section class="container">
			<div style="border-top:dotted 3px orange;"></div>
			<div style="padding:10px 0 5px 0">
				<div class="columns">
					<div class="column col-md-6">
						<p style="text-transform:uppercase;font-size:20px;font-weight:bold;color:#ff550f;" class="float-left">open registration intake 2016</p>
					</div>
					<div class="column col-md-6">
						<p style="" class="float-right"><span style="font-size:20px;text-transform:capitalize;">call us now!</span> <span style="font-size:23px;color:#ff550f;font-weight:bold;">+603-3381 1766</span></p>
					</div>
				</div>
			</div>
			<div style="border-top:dotted 3px orange;"></div>
		</section>
		<section class="container slider-section">						
			<div class="slider-content-1" style="width:70%;height:70%">
				<ul class="bxslider2" style="margin:0;">
					<li><img src="img/slide-content-1.png" title="Funky roots" /></li>
					<li><img src="img/slide-content-1.png" title="The long and winding road" /></li>
					<li><img src="img/slide-content-1.png" title="Happy trees" /></li>
				</ul>
			</div>			
			<div class="slider-content-2" style="width:40%;height:40%">
				<ul class="bxslider2" style="margin:0;">
					<li><img src="img/slide-content-2.png" title="Funky roots"/></li>
					<li><img src="img/slide-content-2.png" title="The long and winding road" /></li>
					<li><img src="img/slide-content-2.png" title="Happy trees" /></li>
				</ul>
			</div>
			<div class="slider-content-3" style="width:60%;height:60%">
				<ul class="bxslider3" style="margin:0;" >
					<li><img src="img/slide-content-3.png" title="Funky roots"/></li>
					<li><img src="img/slide-content-3.png" title="The long and winding road" /></li>
					<li><img src="img/slide-content-3.png" title="Happy trees" /></li>
				</ul>
			</div>
			<div class="slider-content-4"  style="width:70%;height:70%">
				<ul class="bxslider4" style="margin:0;">
					<li><img src="img/slide-content-4.png" title="Funky roots"/></li>
					<li><img src="img/slide-content-4.png" title="The long and winding road" /></li>
					<li><img src="img/slide-content-4.png" title="Happy trees" /></li>
				</ul>
			</div>			
		</section>
		<?php include('partials/footer.php'); ?>
	</body>
</html>