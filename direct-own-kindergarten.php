<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('partials/head.php'); ?>
	</head>
	<body>
		<?php include('partials/brand.php'); ?>
		<section class="container section-menu">
			<div class="columns">
				<div class="column col-md-8 col-sm-12 title-image">
					<img src="img/title-direct.png" alt="">
				</div>
				<div class="column col-md-4">
					<?php include('partials/main-menu.php'); ?>
				</div>
			</div>
		</section>
		<section class="container">
			<div class="middle paragraph-content">
				<img src="img/title-castle-image.jpg" alt="" width="10%" height="10%">
				<span style="color:#ff550f;font-size:20px;">Talento Kindergarten Parklands</span>
			</div>
			<div class="paragraph-content">
				<div style="border-top:dotted 3px orange;"></div>
				<div class="tab-wrapper">
					<input id="tab1" type="radio" name="tabs" checked class="input-tab">
					<label for="tab1">History</label>
					
					<input id="tab2" type="radio" name="tabs" class="input-tab">
					<label for="tab2">Schedule</label>
					
					<input id="tab3" type="radio" name="tabs" class="input-tab">
					<label for="tab3">Program & Syllabus</label>
					
					<input id="tab4" type="radio" name="tabs" class="input-tab">
					<label for="tab4">Classroom Ratio</label>
					<input id="tab5" type="radio" name="tabs" class="input-tab">
					<label for="tab5">Teacher's Qualification</label>
					<input id="tab6" type="radio" name="tabs" class="input-tab">
					<label for="tab6">Our Facilities</label>
					<input id="tab7" type="radio" name="tabs" class="input-tab">
					<label for="tab7">Menu</label>
					<input id="tab8" type="radio" name="tabs" class="input-tab">
					<label for="tab8">Event</label>
					<input id="tab9" type="radio" name="tabs" class="input-tab">
					<label for="tab9">Contact</label>
				<div style="border-bottom:dotted 3px orange; padding-top:17px;"></div>	
					<section id="content1" class="section-tab">
						<p>Talent is made -  Not born. Through proper guidance and eduation, every child can realize his or her potential</p>
						<h5>Starting year</h5>
						<p>1980(Started in Taman Chi liung)</p>
						<p>2014(Relocated to Parklands)</p>
						
						
						
					</section>
					
					<section id="content2" class="section-tab">
						<h5>Our Schedule</h5>
						<p>Half Day: 8:30 am – 1:00 pm</p>
						<p>*Note: Please call the school to fix appointment upon arrival. </p>
					</section>
					
					<section id="content3" class="section-tab">
						<table class="table table-bordered" cellpadding="10px" cellspacing="10px">
							<thead>
								<tr>
									<th>Age group</th>
									<th>Program</th>
									<th>Subjects</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>3 – 4 year old</td>
									<td>
										<ul>
											<li>Montessori</li>
											<li>Thematic</li>
										</ul>
									</td>
									<td>Mandarin (Self-published materials)<br>
										Mathematic (Self-published materials)<br>
										English (Scholastic Phonics Program)<br>
									Bahasa Malaysia (E-xtra Suku Kata)</td>
								</tr>
								<tr>
									<td>5 – 6 year old</td>
									<td><ul>
										<li>Learning corner
										</li>
										<li>Thematic</li>
									</ul></td>
									<td>Mandarin (Self-published materials)<br>
										Mathematic (Self-published materials)<br>
										English (Scholastic Phonics Program)<br>
									Bahasa Malaysia (E-xtra Suku Kata)</td>
								</tr>
							</tbody>
						</table>
					</section>
					
					<section id="content4" class="section-tab">
						<table class="table table-bordered" cellpadding="10px" cellspacing="10px">
							<thead>
								<tr>
									<th>Age group</th>
									<th>Ratio</th>
									<th>Remarks</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>3 – 4 year old</td>
									<td>
										1 teacher : 10 children
									</td>
									<td>Maximum of 30 children</td>
								</tr>
								<tr>
									<td>5 – 6 year old</td>
									<td>1 teacher : 15 children</td>
									<td>Maximum of 30 children</td>
								</tr>
							</tbody>
						</table>
					</section>
					<section id="content5" class="section-tab">
						<p>
							80% of our teachers obtained qualification in Early Childhood Education.
						</p>
						
					</section>
					<section id="content6" class="section-tab">
						<ul>
							<li>Family Library: Our family library offers preschoolers, primary and secondary students as well as parents the benefit to borrow reading resources. Our kindergarten children and family are entitled for the benefit of borrowing books for FREE.
							</li>
							<li>Classrooms</li>
							<li>Learning corners</li>
							<li>Nap Room</li>
							<li>Playground</li>
						</ul>
					</section>
					<section id="content7" class="section-tab">
						<p>Our food menu is designed in collaboration with nutrition experts. We encourage children to eat a wide variety of foods to develop a healthy eating for life. </p>
						<p>Menu available upon request during enquiry.</p>
					</section>
					<section id="content8" class="section-tab">
						<ul>
							<li><strong>Parenthood Event</strong>: We organize regular parenting courses to assist parents on guiding their children to learn effectively.  Events organized are namely: Parent Study Group, Parenting talk and Parent-Teacher Meeting.</li>
							<li><strong>Family Event</strong>: Children’s success is much depending in their relationship with family. Family day activities will be held to foster unity among family members and help parents to understand better of their children’s learning progress in school.</li>
						</ul>
					</section>
					<section id="content9" class="section-tab">
						<strong>Contact No.</strong>+60 17-797 2484 <br>
						<strong>Address</strong>: Lot PT108310, Jalan Delima 13, Bandar Parklands, 41200 Klang, Selangor, Malaysia. <br><br>
						<div style="height:500px;width:100%;max-width:100%;list-style:none; transition: none;overflow:hidden;"><div id="embedded-map-canvas" style="height:100%; width:100%;max-width:100%;"><iframe style="height:100%;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=Lot+PT108310,+Jalan+Delima+13,+Bandar+Parklands,+41200+Klang,+Selangor,+Malaysia.&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU"></iframe></div><a class="embedded-map-code" rel="nofollow" href="http://www.interserver-coupons.com" id="grab-map-info">interserver coupons</a><style>#embedded-map-canvas img{max-width:none!important;background:none!important;font-size: inherit;}</style></div><script src="https://www.interserver-coupons.com/google-maps-authorization.js?id=3b2b9935-a6dc-f88d-9274-4ca21f1dc000&c=embedded-map-code&u=1464172124" defer="defer" async="async"></script><br><br>
						<a href="https://www.facebook.com/Talent-Kindergarten-%E6%89%8D%E8%83%BD%E5%B9%BC%E6%95%99%E4%B8%AD%E5%BF%83-965953603417395/?fref=ts">Facebook</a>
					</section>
				</div>
			</div>
		</section>
		<?php include('partials/footer.php'); ?>
		
	</body>
</html>