<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('partials/head.php'); ?>
	</head>
	<body>
		<?php include('partials/brand.php'); ?>
		<section class="container section-menu">
			<div class="columns">
				<div class="column col-md-8 col-sm-12 title-image">
					<img src="img/title-publication.png" alt="">
				</div>
				<div class="column col-md-4">
					<?php include('partials/main-menu.php'); ?>
				</div>
			</div>
		</section>
		<section class="container">
			<div class="paragraph-content">
				<img src="img/content-publication.jpg" alt="" width="70%" height="70%">
			</div>
		</div>
		
	</section>
	
	<?php include('partials/footer.php'); ?>
	
</body>
</html>