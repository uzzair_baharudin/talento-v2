<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('partials/head.php'); ?>
	</head>
	<body>
		<?php include('partials/brand.php'); ?>
		<section class="container section-menu">
			<div class="columns">
				<div class="column col-md-8 col-sm-12 title-image">
					<img src="img/title-teaching.png" alt="">
				</div>
				<div class="column col-md-4">
					<?php include('partials/main-menu.php');?>
				</div>
			</div>
		</section>
		<section class="container section-content">
			<img class="content-img" src="img/content-teaching-approach.jpg" alt="" width="30%" height="30%" style="float:right;" />			
			<p class="paragraph-content">Open Concept Learning Corner By replacing regular learning setting with learning corner without fixed lesson, tables and chairs, learning area can be used flexibly with rich learning resources that allow children to explore freely.</p>
			<img class="paragraph-content" src="img/chinese-teaching-text.jpg" alt="" width="60%" height="60%">
			<h4 class="paragraph-content">Teaching Program</h4>
			<p class="paragraph-content">We emphasize a concept that knowledge comes from active participation in activities. It is acquired through self-discovery, or the process of formulating certain hypothesis and then proving it through experience. <br><br>
				With children's interest, ability and experience as a starting point, we gather data which are consistent with children's holistic development and allow children to self-explore and reorganize experience, develop confidence and work with courage and determination, and discover their potential in learning. <br><br>
				Teachers inspire children verbally and nonverbally to observe children, gather data to reflect on ways to expand children's learning opportunities and work out with hypotheses on how to enhance children's developmental experiences and achieve learning target.
				The content of teaching materials include knowledge, skills, dispositions and feelings.
			</p>
			<img class="paragraph-content" src="img/content-teaching-2.jpg" alt="" width="100%" height="100%">						
		</section>
		<?php include('partials/footer.php'); ?>
		
	</body>
</html>