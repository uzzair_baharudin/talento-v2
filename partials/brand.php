<div class="container section-vertbanner">
	<div class="content-vertbanner">
		<img src="img/vert-banner.png" class="ribbon"/>
	</div>
</div>
<div class="container section-logo">
	<img src="img/logo.png" alt="" class="logo"/>
</div>
<div class="container section-selectmenu">
	<nav class="centered">
		<p><strong>Navigation Menu</strong></p>
		<select onchange="if (this.value) window.location.href = this.value;">
			<option value="philosophy.php">Philosophy</option>
			<option value="index.php">Origin</option>
			<option value="teaching-approach.php">Teaching Approach</option>
			<option value="direct-own-kindergarten.php">Direct Own Kindergarten</option>
			<option value="network-consultation.php">Network Consultation</option>
			<option value="teacher-training-2.php">Teacher's Training</option>
			<option value="publication.php">Publication & Education Materials</option>
			<option value="contact-us.php">Contact Us</option>
		</select>
	</nav>
</div>