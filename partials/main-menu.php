<ul class="menu-main">
	<li><span style="float:left;"><a href="philosophy.php"><img src="img/nav-bullet.png" alt=""></span><span style="">Philosophy</span><br><span class="menu-link-chinese" style="">哲学</span></a></li>
	<li><span style="float:left;"><a href="index.php"><img src="img/nav-bullet.png" alt=""></span><span>Origin</span><br><span class="menu-link-chinese">沿革</span></a></li>
	<li><span style="float:left;"><a href="teaching-approach.php"><img src="img/nav-bullet.png" alt=""></span><span>Teaching Approach</span><br><span class="menu-link-chinese">教学方式</span></a></li>
	<li><span style="float:left;"><a href="direct-own-kindergarten.php"><img src="img/nav-bullet.png" alt=""></span><span>Direct Own Kindergarten</span><br><span class="menu-link-chinese">直属幼儿园</span></a></li>
	<li><span style="float:left;"><a href="network-consultation.php"><img src="img/nav-bullet.png" alt=""></span><span>Network Consultation</span><br><span class="menu-link-chinese">网络咨询</span></a></li>
	<li><span style="float:left;"><a href="teacher-training.php"><img src="img/nav-bullet.png" alt=""></span><span>Teacher's Training</span><br><span class="menu-link-chinese">教学方式</span></a></li>
	<li><span style="float:left;"><a href="publication.php"><img src="img/nav-bullet.png" alt=""></span><span>Publication & Education Materials</span><br><span class="menu-link-chinese">出版刊物</span></a></li>
	<li><span style="float:left;"><a href="contact-us.php"><img src="img/nav-bullet.png" alt=""></span><span>Contact Us</span><br><span class="menu-link-chinese">联系我们</span></a></li>
</ul>