<footer>
	<div class="container section-school-responsive">
		<div class="columns">
			<div class="col-sm-2">
				<img src="img/school2.png" alt="" style="float:left;">
			</div>
			<div class="col-sm-10">
				<p style=""><strong>Mighty Junior, Bukit Tinggi, Klang</strong></p>
				<p style=""><strong>Address</strong>: No.1, Lorong Batu Nilam 9L, Bandar Bukit Tinggi, 41200 Klang, Selangor, Malaysia.</p>
				<p style=""><strong>Contact No</strong>: +603-33241706</p>
			</div>
		</div>
		<br>
		<div class="columns">
			<div class="col-sm-2">
				<img src="img/school1.png" alt="" style="float:left;">
			</div>
			<div class="col-sm-10">
				<p style=""><strong>Taska OUG, Kuala Lumpur</strong></p>
				<p style=""><strong>Address</strong>: No. 39, Jalan Hujan Emas, OUG, 58200 Kuala Lumpur, Malaysia.</p>
				<p style=""><strong>Contact No</strong>: +603-7785 4863</p>
			</div>
		</div>
		<br>
		<div class="columns">
			<div class="col-sm-2">
				<img src="img/school3.png" alt="" style="float:left;">
			</div>
			<div class="col-sm-10">
				<p style=""><strong>Talento Daycare, Taman Chi Liung, Klang</strong></p>
				<p style=""><strong>Address</strong>: No. 22, Jalan Kadok, Taman Chi Liung, 42100 Klang, Selangor, Malaysia.</p>
				<p style=""><strong>Contact No</strong>: +603-3381 1766</p>
			</div>
		</div>
		<br>
		<div class="columns">
			<div class="col-sm-2">
				<img src="img/school4.png" alt="" style="float:left;">
			</div>
			<div class="col-sm-10">
				<p style=""><strong>Tadika Overseas Union, Kuala Lumpur</strong></p>
				<p style=""><strong>Address</strong>: No. 20, Jalan Hujan Batu 1, OUG, 58200 Kuala Lumpur.</p>
				<p style=""><strong>Contact No</strong>: +603-7982 6978</p>
			</div>
		</div>
		<br>
		<div class="columns">
			<div class="col-sm-2">
				<img src="img/school5.png" alt="" style="float:left;">
			</div>
			<div class="col-sm-10">
				<p style=""><strong>Talento Academy</strong></p>
				<p style=""><strong>Address</strong>: No. 22, Jalan Kadok, Taman Chi Liung, Selangor, Malaysia </p>
				<p style=""><strong>Contact No</strong>: +603-3381 1766</p>
			</div>
		</div>
	</div>
	<div class="container section-socialcloud">
		<img src="img/youtube.png" alt="" width="10%" height="10%">
		<img src="img/facebook.png" alt="" width="10%" height="10%">
	</div>
	<div class="container section-school">
		<div class="school1"><img id="school1" src="img/transparent.png" alt=""/></div>
		<div class="school2"><img id="school2" src="img/transparent.png" alt=""/></div>
		<div class="school3"><img id="school3" src="img/transparent.png" alt=""/></div>
		<div class="school4"><img id="school4" src="img/transparent.png" alt=""/></div>
		<div class="school5"><img id="school5" src="img/transparent.png" alt=""/></div>
	</div>
	<div class="container section-selectmenu-sitemap">
		<nav class="centered">
			<p><strong>Sitemap Navigation</strong></p>
			<select onchange="if (this.value) window.location.href = this.value;">
				<option value="index.php">Philosophy</option>
				<option value="">Origin</option>
				<option value="">Teaching Approach</option>
				<option value="">Direct Own Kindergarten</option>
				<option value="">Network Consultation</option>
				<option value="">Teacher's Training</option>
				<option value="">Publication & Education Materials</option>
				<option value="">Contact Us</option>
			</select>
		</nav>
	</div>
	<div class="container section-footermenu">
		<div class="columns">
			<div class="col-md-3 col-sm-12">
				<p class="title-sitemap">Sitemap</p>
				<ul class="footer-menu">
					<li><a href="">Philosophy</a></li>
					<li><a href="">Origin</a></li>
					<li><a href="">Teaching Approach</a></li>
					<li><a href="">Direct Own Kindergarten</a></li>
				</ul>
			</div>
			<div class="col-md-4 col-sm-12">
				<ul class="footer-menu">
					<li><a href="">Network Consultation</a></li>
					<li><a href="">Teacher's Training</a></li>
					<li><a href="">Publication & Education Materials</a></li>
					<li><a href="">Contact Us</a></li>
				</ul>
			</div>
			<div class="col-md-5 col-sm-12">
				<p>Sign up for our newsletter and receive special updates.</p>
				<div class="input-group">
					<input type="text" class="form-input input-subscribe" placeholder="Enter your email address here"/>
					<button class="btn btn-subscribe input-group-btn">Subscribe</button>
				</div>
				<div class="section-appbutton">
					<img src="img/ios-button.jpg" alt="" width="100" height="35"/>
					<img src="img/gp-button.jpg" alt="" width="100" height="35"/>
				</div>
			</div>
		</div>
	</div>
	<section id="footer-navbar">
		<div class="container">
			<div class="columns">
				<div class="col-md-12 centered">
					<p class="footer-block text-center">copyright@talentoschool2016</p></div>
				</div>
			</div>
		</section>
	</footer>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
	<script src="bubble.js"></script>
	<script type="text/javascript" src="jquery.bxslider.js"></script>
	<script>
		$('.bxslider').bxSlider({
		mode: 'fade',
		auto:true,
		slideSelector: '',
		
	});
		$('.bxslider2').bxSlider({
		mode: 'fade',
		auto:true,
		controls: false,
		pager:false
		
	});
		$('.bxslider3').bxSlider({
		mode: 'fade',
		auto:true,
		controls: false,
		pager:false
		
	});
		$('.bxslider4').bxSlider({
		mode: 'fade',
		auto:true,
		controls: false,
		pager:false
		
	});
	</script>
	<script>
		$('#school2').sBubble({
	content: 'TASKA OUG<br>No. 39, Jalan Hujan Emas,<br>OUG, 58200<br>Kuala Lumpur, Malaysia<br>+603–7785 4863',
	height: '100px',
	width: '200px',
	theme:'myTheme'
	});
		$('#school3').sBubble({
	content: 'TALENTO DAYCARE<br>No. 22, Jalan Kadok,<br> Taman Chi Liung, 42100<br>Klang, Selangor, Malaysia<br>603–3381 1766',
	height: '100px',
	width: '200px',
	theme:'myTheme'
	});
		$('#school4').sBubble({
	content: 'TADIKA OVERSEAS UNION<br>No. 39, Jalan Hujan Emas, OUG, 58200<br> Kuala Lumpur, Malaysia<br>+603–7785 4863',
	height: '100px',
	width: '200px',
	theme:'myTheme'
	});
		$('#school1').sBubble({
	content: 'MIGHTY JUNIOR<br>No.1, Lorong Batu Nilam 9L,<br> Bandar Bukit Tinggi,<br> 41200 Klang,<br> Selangor, Malaysia<br>+603-33241706',
	height: '120px',
	width: '200px',
	theme:'myTheme'
	});
		$('#school5').sBubble({
	content: 'TALENTO ACADEMY <br>No. 22, Jalan Kadok,<br> Taman Chi Liung,<br>Selangor, Malaysia <br>+603-3381 1766 ',
	height: '100px',
	width: '200px',
	theme:'myTheme'
	});
	</script>