<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('partials/head.php'); ?>
	</head>
	<body>
		<?php include('partials/brand.php'); ?>
		<section class="container section-menu">
			<div class="columns">
				<div class="column col-md-8 col-sm-12 title-image">
					<img src="img/title-network.png" alt="">
				</div>
				<div class="column col-md-4">
					<?php include('partials/main-menu.php'); ?>
				</div>
			</div>
		</section>
		<section class="container">
			<img class="content-img" src="img/content-philosophy.png" alt="" width="30%" height="30%" style="float:right;"/>
			<div class="paragraph-content">
				<h3>Curriculum Design 课程规划 </h3>
				<p>
					Talent is not born. Through proper guidance and education, every child can realize his or her potential. We believe that, by inspiring a child's intrinsic learning motive, there will not be a child who cannot learn. <br><br>
					We believe that, by moving a child with "love", using fluent language, a beautiful environment, and a tender heart, there will not be a child who cannot be taught. To inspire the child's intrinsic learning motive,allow the child to learn through "application", learn through practical life experience. <br><br>
					The child must be the "master" of their own learning and be able to learn voluntarily. We believe that, with the ability to read, the child would be able to acquire a variety of abilities, face up to challenges, and overcome obstacles. <br><br>
					The curriculum gives an equal emphasis on moral, cognitive, physical, social, and art.
					The teacher inspires the child with words and actions, sets themes that appeal to the child, gathers information that relates to child's physical and cognitive development and provides room for the child to explore. <br><br>
					To accentuate children's ability by cultivating language ability, practical life skills, social ability, technology capability, as well as the ability to care for people and things. In order to face up to challenges in the ever changing world, we also give an emphasis on the appreciation of "beauty", guiding them towards appreciating Chinese culture, respecting others customs and traditions, possessing an acceptance for various cultures. <br> <br>
					The teaching embraces multiple intelligences, which suits children of different learning styles.
				</p>
			</div>
		</section>
		<section class="container">
			<img src="img/content-network-2.png" width="30%" height="30%" alt="" class="content-img-2" style="float:left;" />
			<div class="paragraph-content">
				<h4>Teaching Training 师资培习l} </h4>
				<p>
					Teacher is one of the important elements of a preschool because they facilitate children's learning. <br><br>
					Talento Academy has been organizing series of teachers' training workshop that establish on basic teaching philosophy and at the same time to equip teachers with practical learning experience, knowledge and skills to make educating young children more interesting and lively. From the workshop, teachers will have the opportunity to be updated on the National Standard Preschool Curriculum. <br><br>
					Furthermore, teachers will be benefited with the knowledge and experiences shared by the speakers and get inspiration on various ways to impose new ideas and teaching method into their lesson. <br><br>
					Other than that, Talento also organizes teachers' training with different title focusing on different learning areas of children. Teachers may take the opportunity to participate and explore in depth to develop further on their desire area. For further enquiry, please click.
				</p>
			</div>
		</section>
		<section class="container">
			<div class="paragraph-content">
				<h4>Management 行政管理</h4>
				<p>Talento provide services on setting up or improvising school management system. We would apply ISO business managing standard to ensure efficiency in school management system. We would also work out on labeling school management folders and document with code and organize them accordingly. <br><br>
				Other than that, we design staff salary scale, staff evaluation criteria and provide teachers' training. For further enquiry, please click 0 </p>
			</div>
		</section>
		<section class="container">
			<div class="paragraph-content">
				<h4>Planning Activities 亲职教育</h4>
				<p>It is not only teachers job to education the children but parents hold huge responsibilities in education children. Parental involvement is essential and closely related to the success of children's learning. Parents are encouraged to participate in parenting activities organized by the school and also take it as the opportunity to understand their children better. <br><br>
					Talento work together with schools in planning and organizing a series of parenting activities. Parents may take the opportunity to explore further on the knowledge and skills in understanding and ways to handle their children. For further enquiry, please click,
				</p>
			</div>
		</section>
		<section class="container">
			<div class="paragraph-content">
				<h4>Licensing 营业执照</h4>
				<p>Talento provides a series of services on license application namely:</p>
				<ul class="">
					<li>Design comprehensive proposal for all agencies. <br>
					(Local Council, Ministry of Health, File Department, State Ministry of Education)
					 </li>
					<li>Follow up on license application status</li>
					<li>Arrange appointment for inspection visit by relevant agencies</li>
					<li>Application for teaching permit</li>
				</ul>
			</div>
		</section>
		<?php include('partials/footer.php'); ?>
		
	</body>
</html>