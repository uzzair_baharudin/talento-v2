<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('partials/head.php'); ?>
	</head>
	<body>
		<?php include('partials/brand.php'); ?>
		<section class="container section-menu">
			<div class="columns">
				<div class="column col-md-8 col-sm-12 title-image">
					<img src="img/title-teacher-training.png" alt="">
				</div>
				<div class="column col-md-4">
					<?php include('partials/main-menu.php'); ?>
				</div>
			</div>
		</section>
		
		<section class="container" style="padding-top:20px;">
			<div class="paragraph-content">
				<div style="border-top:dotted 3px orange;"></div>
				<div class="tab-wrapper">
					<input id="tab1" type="radio" name="tabs" checked class="input-tab">
					<label for="tab1">2015 Go Green</label>
					
					<input id="tab2" type="radio" name="tabs" class="input-tab">
					<label for="tab2">Album 2</label>
					
					<input id="tab3" type="radio" name="tabs" class="input-tab">
					<label for="tab3">Album 3</label>
					<div style="border-bottom:dotted 3px orange; padding-top:17px;"></div>
					<section id="content1" class="section-tab">
						<div class="container">
							<div class="columns">
								<div class="col-md-3"><img src="http://placehold.it/250x150"></div>
								<div class="col-md-3"><img src="http://placehold.it/250x150"></div>
								<div class="col-md-3"><img src="http://placehold.it/250x150"></div>
							</div>
						</div>
					</section>
					
					<section id="content2" class="section-tab">
						<div class="container">
							<div class="columns">
								<div class="col-md-3"><img src="http://placehold.it/250x150"></div>
								<div class="col-md-3"><img src="http://placehold.it/250x150"></div>
								<div class="col-md-3"><img src="http://placehold.it/250x150"></div>
							</div>
						</div>
					</section>
					
					<section id="content3" class="section-tab">
						<div class="container">
							<div class="columns">
								<div class="col-md-3"><img src="http://placehold.it/250x150"></div>
								<div class="col-md-3"><img src="http://placehold.it/250x150"></div>
								<div class="col-md-3"><img src="http://placehold.it/250x150"></div>
							</div>
						</div>
					</section>
					
					
				</div>
			</div>
		</section>
		
		
		
		<section class="container">
			
		</section>
		<?php include('partials/footer.php'); ?>
		
	</body>
</html>