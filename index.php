<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('partials/head.php'); ?>
	</head>
	<body>
		<?php include('partials/brand.php'); ?>
		<section class="container section-menu">
			<div class="columns">
				<div class="column col-md-8 col-sm-12 title-image">
					<img src="img/title-origin.png" alt="">
				</div>
				<div class="column col-md-4">
					<?php include('partials/main-menu.php'); ?>
				</div>
			</div>
		</section>
		<section class="container">
			<img class="img-content-origin" src="img/content-origin.png" alt="" width="30%" height="30%" style="float:right;margin:0 0 10px 10px;">
			<p class="paragraph-content">
				Founded in 2009, Talento Academy is a comprehensive early childhood education organisation in Malaysia actively promoting open preschool education and building communication platforms to facilitate Malaysian educators to improve their expertise and teaching skills. <br><br>
				In 2009, Talento Academy strives to promote reading programmes and build up reading culture by co-organising the "100 reading wishes" program with preschools to encourage reading at a young age. <br><br>
				In 2010, Talento Academy set up a "parenting education department" to highlight the importance of parenting education and help parents take parent-child interaction seriously and tighten the parent-child relationship by organising regular parent-child events and parenting courses every year. <br><br>
				In 2010, Talento Academy was invited to collaborate with a special unit in the Malaysian Prime Minister's Office in planning the 10-year transformation blueprint for preschool education (2010-2020) under Malaysia's National Key Economic Areas (NKEA).<br> Today, Talento Academy assisted more than 30 preschools to implement plans for administration, curriculum and teaching transformation.<br><br>
				In 2011, Talento Academy co-organized with part-time holiday courses with universities in Taiwan and invited experts and scholars to conduct lectures in Malaysia to enhance local preschool teachers' theoretical foundation and teaching skills. In order to expand local preschool educators' vision, Talento Academy organises overseas preschool visitations on a regular basis to allow local and overseas preschool educators to observe and exchange their teaching experience. <br><br>
				In 2012, Talento Academy compiled and published a series of text-only readers and intellectual training worksheets. <br><br>
				In 2013 and 2014, Talento Academy held the first and second Practical and Thinking Workshop for Pre-School Education. The workshop, organized in both Kuala Lumpur and Sarawak (Bahagian Sibu) in 2014, gathered around 700 local and overseas preschool experts, scholars and educators to brainstorming, discuss and share their teaching experience to enhance Malaysia's preschool education quality and lay an essential foundation for preschool teacher's professional development.
			</p>
		</div>
		
	</section>
	<section class="container">
		<div class="paragraph-content">
			<img src="img/content-origin-2.png" width="25%" height="25%" alt="" class="content-img-2" style="float:left;">
			<img src="img/chinese-origin-text.jpg" width="60%" height="60%" alt="" class="content-img-2">
		</div>
	</section>
	<?php include('partials/footer.php'); ?>	
</body>
</html>