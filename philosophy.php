<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('partials/head.php'); ?>
	</head>
	<body>
		<?php include('partials/brand.php'); ?>
		<section class="container section-menu">
			<div class="columns">
				<div class="column col-md-8 col-sm-12 title-image">
					<img src="img/title-philosophy.png" alt="">
				</div>
				<div class="column col-md-4">
					<?php include('partials/main-menu.php');?>
				</div>
			</div>
		</section>
		<section class="container section-content">
			<img class="img-content-origin" src="img/content-philosophy.png" alt="" width="30%" height="30%" style="float:right;margin:0 0 10px 10px;">
			<p class="paragraph-content">
				Talent is not born. Through proper guidance and education, every child can realize his or her potential. We believe that, by inspiring a child's intrinsic learning motive, there will not be a child who cannot learn. <br><br>
				We believe that, by moving a child with "love", using fluent language, a beautiful environment, and a tender heart, there will not be a child who cannot be taught. To inspire the child's intrinsic learning motive,allow the child to learn through "application", learn through practical life experience. <br><br>
				The child must be the "master" of their own learning and be able to learn voluntarily. We believe that, with the ability to read, the child would be able to acquire a variety of abilities, face up to challenges, and overcome obstacles. <br><br>
				The curriculum gives an equal emphasis on moral, cognitive, physical, social, and art.
				The teacher inspires the child with words and actions, sets themes that appeal to the child, gathers information that relates to child's physical and cognitive development and provides room for the child to explore. <br><br>
				To accentuate children's ability by cultivating language ability, practical life skills, social ability, technology capability, as well as the ability to care for people and things. In order to face up to challenges in the ever changing world, we also give an emphasis on the appreciation of "beauty", guiding them towards appreciating Chinese culture, respecting others customs and traditions, possessing an acceptance for various cultures. <br> <br>
				The teaching embraces multiple intelligences, which suits children of different learning styles.
			</p>
		</div>
	</section>
	<section class="container">
<p class="paragraph-content">
	<img src="img/content-2.png" width="40%" height="40%" alt="" class="content-img-2"/>
	<img src="img/chinese-philosophy-text.jpg" alt="" width="55%" height="55%"/>
</p>
	</section>
	<?php include('partials/footer.php'); ?>
	
</body>
</html>