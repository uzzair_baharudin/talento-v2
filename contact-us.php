<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include('partials/head.php'); ?>
	</head>
	<body>
		<?php include('partials/brand.php'); ?>
		<section class="container section-menu">
			<div class="columns">
				<div class="column col-md-8 col-sm-12 title-image">
					<img src="img/title-contact-us.png" alt="">
				</div>
				<div class="column col-md-4">
					<?php include('partials/main-menu.php');?>
				</div>
			</div>
		</section>
		<section class="container">
			<div class=""">
				<img src="img/content-philosophy.png" alt="" width="30%" height="30%" style="float:right;clear:left;" />
				<div class="paragraph-content" style="overflow:hidden;">
					<h4>Talento Daycare, Taman Chi Liung, Klang</h4>
					<p>Contact:+603-3381 1766</p>
					<p>Address: No. 22, Jalan Kadok, Taman Chi Liung <br>42100 Klang, Selangor, Malaysia.</p>
					<p>Email: info@talento.com.my</p>
					<p>Facebook: <a href="https://www.facebook.com/Talentoacademy/">https://www.facebook.com/Talentoacademy/</a></p>
					<p class="paragraph-content">
						<form>
							<!-- name -->
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user "></i></span>
									<input class="form-input" type="text" id="input-example-1" placeholder="Your Name" />
								</div>
							</div>
							<!-- subject -->
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope "></i></span>
									<input class="form-input" type="text" id="input-example-1" placeholder="Your Email Address" />
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-pencil"></i></span>
									<input class="form-input" type="text" id="input-example-1" placeholder="Subject" />
								</div>
							</div>
							<!-- form textarea control -->
							<div class="form-group">								
								<textarea class="form-input" id="input-example-3" placeholder="Your Message..." rows="3"></textarea>
							</div>
							<div class="form-group">
								<button class="btn btn-send float-right"><span class="fa fa-envelope"></span> Submit</button>
							</div>
						</form>
					</p>
				</div>
			</div>
			
		</section>
		<section class="container">
			<div class="paragraph-content">
				<img src="img/contact-working-us.jpg" alt="" width="30%" height="30%"/>
				<img src="img/content-network-2.png" alt="" style="float:right;" width="30%" height="30%">
				<div class="" style="overflow:hidden;">
				<p>We are looking for inspiring candidates who are passionate about making a change in education! If you are a dynamic professional and have been looking for a chance to make a difference by inspiring young individuals, we welcome you to send your resume to info@talento.com.my </p>
				<p>Vacancy in Talent Kindergarten Parklands, Kiang Academic Staff 1.Class Teacher for 4 Year Old •</p>
				<p>(Job description)</p>
				<p>Non Academic staff: </p>
				<h5 class="title-vacancy">Vacancy in Mighty Junior Bukit Tinggi, Klang</h5>
				<p>There are currently no vacancies available. Thank you. </p>
				<h5 class="title-vacancy">
				Vacancy in Talento Daycare, Taman Chi Liung, Klang</h5>
				<p>There are currently no vacancies available. Thank you. </p>
				<h5 class="title-vacancy">
				Vacancy in Tadika Overseas Union</h5>
				<p> There are currently no vacancies available. Thank you. </p>
				Vacancy in Tadika Overseas Union There are currently no vacancies available. Thank you.
				All applicants should send a copy of cover letter and resume to info@talento.com.my. We thank all applicants for their interest; however, only short-listed applicants will be arranged for interview session.
			</p>
			</div>
		</div>
	</section>
	<?php include('partials/footer.php'); ?>
	
</body>
</html>